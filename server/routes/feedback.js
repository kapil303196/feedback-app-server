var express = require('express')
var router = express.Router()
var {
  authenticate
} = require('../middleware/authenticate');

var {
  Feedback
} = require('../models/feedback');
var {
  Customer
} = require('../models/customer');

const _ = require('lodash');



// ---------- Add Feedback ------------------
router.post('/api/feedback', (req, res) => {

  console.log("Add Feedback for Exising Customer...");

  var feedback = new Feedback({
    UserID: req.body.UserID,
    CustomerID: req.body.CustomerID,
    Feedback: req.body.Feedback,
    FeedbackText: req.body.FeedbackText
  });

  feedback.save().then((info) => {
    console.log("Feedback Added - Success.");
    res.json("Success");
  }, (e) => {
    console.log("Error Adding Feedback.");
    res.status(400).send(e);
  });
});

// ---------- Edit Route ------------------
router.put('/api/route', authenticate, (req, res) => {

  Route.update({
    _id: req.body.RouteID
  }, {
    From: req.body.From,
    To: req.body.To,
    Type: req.body.Type,
    // PickUpTime: req.body.PickUpTime,
    // DropTime: req.body.DropTime,
    TotalDailyTrips: req.body.TotalDailyTrips,
    FirstTrip: req.body.FirstTrip,
    LastTrip: req.body.LastTrip,
    Cost: req.body.Cost,
    Seats: req.body.Seats
  }).then((info) => {
    res.json("Success");
  }, (e) => {
    res.status(400).send(e);
  });
});

// ------------- Get All Routes ---------------------
router.get('/api/routes', authenticate, (req, res) => {
  Route.find().populate('DriverID').sort({
    CreatedOn: -1
  }).then((info) => {
    res.send({
      info
    });
  }, (e) => {
    res.status(400).send(e);
  });
});

// ------------- Get Feedbacks for UserID ---------------------
router.get('/api/feedbacksbyuser/:id', (req, res) => {
  Feedback.find({
    UserID: req.params.id
  }).sort({
    CreatedOn: -1
  }).populate('UserID').populate('CustomerID').then((info) => {
    res.send({
      info
    });
  }, (e) => {
    res.status(400).send(e);
  });
  8
});

// ------------- Get Feedbacks for CustomerID ---------------------
router.get('/api/feedbacksbycustomer/:id', (req, res) => {
  Feedback.find({
    CustomerID: req.params.id
  }).sort({
    CreatedOn: -1
  }).populate('UserID').populate('CustomerID').then((info) => {
    res.send({
      info
    });
  }, (e) => {
    res.status(400).send(e);
  });
});

// ------------- Get One Route ---------------------
router.get('/api/route/:id', authenticate, (req, res) => {
  Route.find({
    _id: req.params.id
  }).sort({
    CreatedOn: -1
  }).populate('DriverID').then((info) => {
    res.send({
      info
    });
  }, (e) => {
    res.status(400).send(e);
  });
});

// ---------------- Delete Route ----------------------
router.delete('/api/route/:id', authenticate, (req, res) => {
  var id = req.params.id;

  Route.findByIdAndRemove(id).then((info) => {
    if (!info) {
      return res.status(404).send();
    }

    res.json("Success");
  }).catch((e) => {
    res.status(400).send();
  });
});

//  ------------------ Delete All Routes ----------------
router.delete('/api/deletenotices', authenticate, (req, res) => {

  console.log("Delete All Routes API called...");

  Route.remove({}).then((info) => {
    console.log("Route Deleted Successfully.");
    res.send({
      info
    });
  }).catch((e) => {
    console.log("Error.");
    res.status(400).send();
  });
});

// ----------------- Search Routes ---------------------

router.get('/api/search-routes/', authenticate, (req, res) => {
  Route.find({
    $or: [{
        From: req.query.From,
        To: req.query.To,
        Type: req.query.Type,
        Seats: {
          $gt: 0
        }
      },
      {
        From: req.query.To,
        To: req.query.From,
        Type: req.query.Type,
        Seats: {
          $gt: 0
        }
      }
    ]
  }).populate('DriverID').sort({
    CreatedOn: -1
  }).then((info) => {

    Stop.find({
      $or: [{
          From: req.query.From,
          To: req.query.To
        },
        {
          From: req.query.To,
          To: req.query.From
        }
      ]
    }).populate('RouteID').sort({
      CreatedOn: -1
    }).then((info) => {
      res.send({
        info
      });
    }, (e) => {
      res.status(400).send(e);
    });

    // res.send({info});
  }, (e) => {
    res.status(400).send(e);
  });
});


module.exports = router
