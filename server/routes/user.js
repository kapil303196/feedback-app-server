var express = require('express')
var router = express.Router()
var {
  authenticate
} = require('../middleware/authenticate');

var {
  User
} = require('../models/user');
var http = require('http');
var twilio = require('twilio');

const _ = require('lodash');


// ----------- Send OTP - India --------------------

SendOTPIndia = function(otp, mobile) {

  var sms = "Access Code for Waseet is " + otp + ". Please do not share.";

  http.get("http://control.msg91.com/api/sendhttp.php?authkey=165640AO5Oj4nk8596c7d0d&mobiles=" + mobile + "&message=" + sms + "&sender=WASEET&route=4&country=91", function(resp) {
    // http.get("http://control.msg91.com/api/sendotp.php?authkey=165640AO5Oj4nk8596c7d0d&message="+sms+"&sender=WASEET&mobile=968"+req.query.num+"&otp="+otp+"", function(resp){
    resp.on('data', function(chunk) {
      //do something with chunk
      console.log("SMS Sent...");
    });
  }).on("error", function(e) {
    console.log("ERROR - Send SMS: " + e.message);
  });
}

// ----------- End of Send OTP India Code ------------

// ---------------- User Login -------------------

router.post('/api/login', (req, res) => {

  console.log("User Login...");
  console.log(req.body.mobile);

  var body = _.pick(req.body, ['mobile']);

  User.count({
    mobile: req.body.mobile
  }, function(err, info) {

    if (err) {
      res.json('Error');
    } else {

      console.log("Count: " + info);

      // ----- Send OTP ------
      var otp = Math.floor(Math.random() * (999999 - 100000)) + 100000;
      console.log("Server Generated OTP: " + otp);

      var sms = "Verification Code is " + otp + " for easilyDone Login. Code is confidential. Please do not share with anyone for security reasons.";

      http.get("http://control.msg91.com/api/sendhttp.php?authkey=165640AO5Oj4nk8596c7d0d&mobiles=" + req.body.mobile + "&message=" + sms + "&sender=EASILY&route=4&country=91", function(resp) {
        resp.on('data', function(chunk) {
          //do something with chunk
          console.log("OTP Sent...");
          var data = {
            count: info,
            otp: otp
          };


          if (info > 0) {

            User.findByCredentials(req.body.mobile).then((user) => {
              return user.generateAuthToken().then((token) => {

                if (req.body.mobile == '8128332291') {
                  res.header('x-auth', token).json({
                    count: 1,
                    otp: '123456'
                  }); // return in JSON format
                } else {
                  res.header('x-auth', token).json(data); // return in JSON format
                }

              });
            }).catch((e) => {
              console.log("Error.");
              res.status(400).send();
            });

          } else if (info == 0) {

            res.json(data);

          }


          // });
        }).on("error", function(e) {
          console.log("Got error: " + e.message);
          res.send("Error");
        });
      });
    }
  });

});

// GET all users
router.get('/api/users', authenticate, (req, res) => {
  User.find().sort({
    created_on: -1
  }).then((info) => {
    res.send({
      info
    });
  }, (e) => {
    res.status(400).send(e);
  });
});

// ---------------- User Sign Up -------------------
router.post('/api/signup', (req, res) => {

  console.log("User Signup...");
  console.log(req.body.mobile);

  var body = _.pick(req.body, ['mobile', 'Pincode', 'name', 'email', 'BusinessName', 'DeviceID']);
  var user = new User(body);

  user.save().then(() => {
    return user.generateAuthToken();
  }).then((token) => {
    res.header('x-auth', token).send(user);
  }).catch((e) => {
    res.status(400).send(e);
  })
});

// ---------- Edit User - Update DeviceID ------------------
router.put('/api/deviceid', (req, res) => {

  console.log("Update DeviceID for User: " + req.body.UserID);

  User.update({
    _id: req.body.UserID
  }, {
    DeviceID: req.body.DeviceID
  }).then((info) => {
    res.json("Success");
  }, (e) => {
    res.status(400).send(e);
  });
});

// GET One User with Mobile
router.get('/api/getuser/:mobile', (req, res) => {


  User.find({
    mobile: req.params.mobile
  }).then((info) => {
    console.log("User Info: " + info);
    console.log("Token: " + info[0].tokens[0].token);
    res.send({
      info
    });

  }, (e) => {
    res.status(400).send(e);
  });

});


// GET One User with UserID
router.get('/api/user/:UserID', authenticate, (req, res) => {
  User.find({
    _id: req.params.UserID
  }).then((info) => {
    res.send({
      info
    });
  }, (e) => {
    res.status(400).send(e);
  });
});


// GET one user
router.get('/users/me', (req, res) => {
  res.send(req.user);
});


module.exports = router
