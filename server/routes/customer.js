var express = require('express')
var router = express.Router()
var {
  authenticate
} = require('../middleware/authenticate');

var {
  Customer
} = require('../models/customer');
var {
  Feedback
} = require('../models/feedback');

const _ = require('lodash');

// ---------- Create Customer ------------------
router.post('/api/customer', (req, res) => {

  console.log("Create New Customer...");
  console.log(req.body.Feedback);
  var body = _.pick(req.body, ['UserID', 'Name', 'Mobile', 'Birthdate']);

  var customer = new Customer(body);

  customer.save().then((info) => {
    console.log("New Customer Created - Success.");

    // ----------- Adding Feedback --------------------

    var feedback = new Feedback({
      UserID: req.body.UserID,
      CustomerID: info._id,
      Feedback: req.body.Feedback,
      FeedbackText: req.body.FeedbackText
    })

    feedback.save(function(err, info) {
      if (err)
        console.log("Error");

      // console.log("Feedback Added");
      res.json("Success");
    });


  }, (e) => {
    console.log("Error Creating New Customer.");
    res.status(400).send(e);
  });
});


// ---------- Edit Route ------------------
router.put('/api/route', authenticate, (req, res) => {

  Customer.update({
    _id: req.body.RouteID
  }, {
    UserID: req.body.UserID,
    Name: req.body.Name,
    Mobile: req.body.Mobile,
    Birthdate: req.body.Birthdate
  }).then((info) => {
    res.json("Success");
  }, (e) => {
    res.status(400).send(e);
  });

});

// ------------- Get All Customers for UserID ---------------------
router.get('/api/customers/:id', (req, res) => {
  Customer.find({
    UserID: req.params.id
  }).populate('UserID').sort({
    CreatedOn: -1
  }).then((info) => {
    res.send({
      info
    });
  }, (e) => {
    res.status(400).send(e);
  });
});


// ------------- Get Routes for DriverID ---------------------
router.get('/api/driver-route/:id', authenticate, (req, res) => {
  Route.find({
    DriverID: req.params.id
  }).sort({
    CreatedOn: -1
  }).populate('DriverID').then((info) => {
    res.send({
      info
    });
  }, (e) => {
    res.status(400).send(e);
  });
});

// ------------- Get One Route ---------------------
router.get('/api/route/:id', authenticate, (req, res) => {
  Route.find({
    _id: req.params.id
  }).sort({
    CreatedOn: -1
  }).populate('DriverID').then((info) => {
    res.send({
      info
    });
  }, (e) => {
    res.status(400).send(e);
  });
});

// ---------------- Delete Route ----------------------
router.delete('/api/route/:id', authenticate, (req, res) => {
  var id = req.params.id;

  Route.findByIdAndRemove(id).then((info) => {
    if (!info) {
      return res.status(404).send();
    }

    res.json("Success");
  }).catch((e) => {
    res.status(400).send();
  });
});

//  ------------------ Delete All Routes ----------------
router.delete('/api/deletenotices', authenticate, (req, res) => {

  console.log("Delete All Routes API called...");

  Route.remove({}).then((info) => {
    console.log("Route Deleted Successfully.");
    res.send({
      info
    });
  }).catch((e) => {
    console.log("Error.");
    res.status(400).send();
  });
});

// ----------------- Search Routes ---------------------

router.get('/api/search-routes/', authenticate, (req, res) => {
  Route.find({
    $or: [{
        From: req.query.From,
        To: req.query.To,
        Type: req.query.Type,
        Seats: {
          $gt: 0
        }
      },
      {
        From: req.query.To,
        To: req.query.From,
        Type: req.query.Type,
        Seats: {
          $gt: 0
        }
      }
    ]
  }).populate('DriverID').sort({
    CreatedOn: -1
  }).then((info) => {

    Stop.find({
      $or: [{
          From: req.query.From,
          To: req.query.To
        },
        {
          From: req.query.To,
          To: req.query.From
        }
      ]
    }).populate('RouteID').sort({
      CreatedOn: -1
    }).then((info) => {
      res.send({
        info
      });
    }, (e) => {
      res.status(400).send(e);
    });

    // res.send({info});
  }, (e) => {
    res.status(400).send(e);
  });
});


module.exports = router
