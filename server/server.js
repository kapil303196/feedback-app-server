require('./config/config');

const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const {
  ObjectID
} = require('mongodb');

var {
  mongoose
} = require('./db/mongoose');

var {
  User
} = require('./models/user');
var {
  authenticate
} = require('./middleware/authenticate');

var app = express();
const port = process.env.PORT;

app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,x-auth");
  res.header('Access-Control-Expose-Headers', 'x-auth');
  next();
});

// ------------------- Include All the Routes ---------------------------

// -------------- Customer Route ------------------
var CustomerRoute = require('./routes/customer')
app.use('/', CustomerRoute)

// -------------- User Route ------------------
var UserRoute = require('./routes/user')
app.use('/', UserRoute)

// -------------- Feedback Route ------------------
var FeedbackRoute = require('./routes/feedback')
app.use('/', FeedbackRoute)


// ------------------------- End of Routes ----------------------------

app.listen(port, () => {
  console.log(`Started up at port ${port}`);
});

module.exports = {
  app
};
