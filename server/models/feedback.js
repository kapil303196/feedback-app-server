var mongoose = require('mongoose');

var Feedback = mongoose.model('Feedback', {
  UserID: {
    type: String,
    required: true,
    ref: 'User',
  },
  CustomerID: {
    type: String,
    required: true,
    ref: 'Customer',
  },
  Feedback: {
    type: Number,
    required: true
  },
  FeedbackText: {
    type: String
  },
  CreatedOn: {
    type: Date,
    default: Date.now
  },
  UpdatedOn: {
    type: Date,
    default: Date.now
  }
});

module.exports = {
  Feedback
};
