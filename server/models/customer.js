var mongoose = require('mongoose');

var Customer = mongoose.model('Customer', {
  UserID: {
    type: String,
    required: true,
    ref: 'User'
  },
  Name: {
    type: String,
    required: true,
    minlength: 3
  },
  Mobile: {
    type: String,
    required: true,
    trim: true,
    minlength: 10,
    unique: true
  },
  Birthdate: {
    type: Date
  },
  CreatedOn: {
    type: Date,
    default: Date.now
  },
  UpdatedOn: {
    type: Date,
    default: Date.now
  }
});

module.exports = {
  Customer
};
