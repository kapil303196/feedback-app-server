const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

var UserSchema = new mongoose.Schema({
  mobile: {
    type: String,
    required: true,
    trim: true,
    minlength: 10,
    unique: true
  },
  BusinessName: {
    type: String,
    minlength: 3,
    required: true
  },
  name: {
    type: String,
    required: true,
    minlength: 3
  },
  Email: {
    type: String,
    minlength: 3
  },
  Pincode: {
    type: Number,
    minlength: 6,
    required: true
  },
  Active: {
    type: Boolean,
    default: true
  },
  DeviceID: {
    type: String
  },
  created_on: {
    type: Date,
    default: Date.now
  },
  updated_on: {
    type: Date
  },
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }]
});



UserSchema.methods.toJSON = function() {
  var user = this;
  var userObject = user.toObject();

  return _.pick(userObject, ['_id', 'mobile', 'name']);
};

UserSchema.methods.generateAuthToken = function() {
  var user = this;
  var access = 'auth';
  var token = jwt.sign({
    _id: user._id.toHexString(),
    access
  }, 'abc123').toString();

  user.tokens = user.tokens.concat([{
    access,
    token
  }]);

  return user.save().then(() => {
    return token;
  });
};

UserSchema.statics.findByToken = function(token) {
  var User = this;
  var decoded;

  try {
    decoded = jwt.verify(token, 'abc123');
  } catch (e) {
    return Promise.reject();
  }

  return User.findOne({
    '_id': decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  });
};

UserSchema.statics.findByCredentials = function(mobile) {
  var User = this;

  return User.findOne({
    mobile
  }).then((user) => {
    if (!user) {
      console.log("User Not Found.");
      return Promise.reject();
    }
    return Promise.resolve(user);
  });
};

var User = mongoose.model('User', UserSchema);

module.exports = {
  User
}
